#!/bin/bash

docker run --restart=always -d --name database -p 5432:5432 -e POSTGRES_USER=dbuser -e POSTGRES_PASSWORD=dbpass postgres:9.6
#!/bin/bash

SCRIPT_FOLDER=$(realpath $0)
PARENT_FOLDER=$(realpath $(dirname ${SCRIPT_FOLDER})/..)
PROJECT_NAME=$(basename ${PARENT_FOLDER})

echo "SCRIPT_FOLDER: ${SCRIPT_FOLDER}"
echo "PARENT_FOLDER: ${PARENT_FOLDER}"
echo "PROJECT_NAME: ${PROJECT_NAME}"

helm template ${PARENT_FOLDER}/helm
helm upgrade --install --wait ${PROJECT_NAME} ${PARENT_FOLDER}/helm